

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path');

var app = express();

// all environments

app.set('port', process.env.PORT || 3002);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("fuckfuck"));
app.use(express.session());
app.use(app.router);
app.use(require('stylus').middleware(__dirname +  '/public'));
app.use(express.static(path.join(__dirname, 'public')));

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.login);
app.get('/login', routes.login);
app.post('/login', routes.postLogin);
app.get('/info', routes.info);
app.get('/superInfo', routes.superInfo);
app.get('/confirm', routes.confirmation);
app.get('/contact', routes.contact);
app.get('/getback', routes.retour);
app.post('/after_confirmed', routes.record_user);
app.post('/alter_confirmed', routes.deny_user);
app.get('/listed', routes.listed);
app.get('/album', routes.album);

app.use(function(req, res, next){
  res.status(404);
  if (req.accepts('html')) {
    res.render('notfound');
    return;
  }
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


