$(document).ready(function(){ 
   
   $('ul.nav > li > a[href="' + document.location.pathname + '"]').parent().addClass('active');
   
   $('body').on('click' , '#retourner' , function(){
      $.ajax({
       type: "GET",
       url: "/info",
       success: function(response){
        $('#coreground').hide().html(response).fadeIn("slow");
        $('#inscription').hide();
       }
      });    
   });

   $('#info').click(function(){
     $.ajax({
       url: "/info/",
       contentType: "html",
       success: function(res){
         $('#coreground').hide().html(res).fadeIn("slow");
        }
     });
   });

   $('#pic').click(function(){
     $.ajax({
       url: "/album",
       success: function(res){
          $('#coreground').hide().html(res).fadeIn("slow");
       }
     });
   });
   
   $('#inscription').click(function(){
     $.ajax({
      url: "/confirm",
      contentType: "html",
      success: function(res){
         $('#coreground').hide().html(res).fadeIn("slow");
      }
     });
   });

  $('body').on("click","#cancel",function(){
    console.log($("#commentaire").val());
     $.ajax({
      url: "/alter_confirmed",
      type: 'POST',
      data: JSON.stringify({commentaire : $("#commentaire").val() }), 
      contentType: 'application/json',  
      success: function(res){
         $('#coreground').hide().html(res).fadeIn("slow");
      }
     });
   });

  $("iframe").wrap('<div class="embed-responsive embed-responsive-16by9"/>');
  $("iframe").addClass('embed-responsive-item');
 });
